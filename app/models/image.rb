class Image < ActiveRecord::Base
  belongs_to :start
  mount_uploader :file, FileUploader
end
