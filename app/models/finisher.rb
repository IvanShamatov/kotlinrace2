class Finisher < ActiveRecord::Base
  has_many :starts
  mount_uploader :photo, PhotoUploader

  def name
    self.first_name + ' ' + self.last_name
  end
end
