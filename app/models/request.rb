class Request < ActiveRecord::Base
  validates_presence_of :name
  validates_presence_of :contact_info
  validates_presence_of :kind
end
