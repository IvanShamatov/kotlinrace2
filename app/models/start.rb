class Start < ActiveRecord::Base
  belongs_to :finisher
  has_many :images
  has_many :reports
end
