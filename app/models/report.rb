class Report < ActiveRecord::Base
  belongs_to :start
  paginates_per 5
  mount_uploader :cover_image, CoverImageUploader
end
