ActiveAdmin.register Finisher do

  permit_params :first_name, :last_name, :middle_name, :country, :city, :age, :gender, :photo, :about, :sport

  form do |f|
    f.inputs do 
      f.input :first_name
      f.input :middle_name
      f.input :last_name
      f.input :photo
      f.input :about
      f.input :sport
      f.input :country
      f.input :city
      f.input :age
      f.input :gender, as: :select, collection: ["Male", "Female"]
    end
    f.actions
  end

end
