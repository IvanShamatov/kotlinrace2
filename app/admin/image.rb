ActiveAdmin.register Image do

  permit_params :file, :start_id

  form do |f|
    f.inputs do 
      f.input :start, as: :select, collection: Start.all.map {|s| ["#{s.finisher.name}  #{s.date}", s.id] }
      f.input :file
    end
    f.actions
  end

end
