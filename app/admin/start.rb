ActiveAdmin.register Start do

  permit_params :finisher_id, :date, :time, :wetsuit, :kind, :whether, :food

  form do |f|
    f.inputs do 
      f.input :finisher, as: :select, collection: Finisher.all.map {|i| ["#{i.first_name} #{i.last_name}", i.id] }
      f.input :date
      f.input :time
      f.input :wetsuit
      f.input :whether
      f.input :food
      f.input :kind, as: :select, collection: ["solo", "relay"]
    end
    f.actions
  end

end
