ActiveAdmin.register Report do

  permit_params :title, :date ,:author, :body ,:cover_image, :description, :start_id

  form do |f|
    f.inputs do 
      f.input :start, as: :select, collection: Start.all.map {|s| ["#{s.finisher.name}  #{s.date}", s.id] }
      f.input :title
      f.input :date
      f.input :author
      f.input :body
      f.input :cover_image
      f.input :description
    end
    f.actions
  end
end
