ActiveAdmin.register Request do
  permit_params :name, :age, :when, :country, :city, :contact_info, :comment
end
