class FinishersController < ApplicationController
  def index
    @finishers = Finisher.all.order(updated_at: :desc)
  end
end
