class PagesController < ApplicationController
  def show
    @page = Page.find_by(url: params[:url])
  end

  def home
  end
end
