class RegisterController < ApplicationController
  def new
    @request = Request.new
  end

  def create
    req = Request.create(request_params)
    if req
      notice = "Заявка принята и будет обработана в ближайшее время. Спасибо"
    end
    redirect_to :back, notice: notice
  end

  private def request_params
    params.require(:request).permit(:name, :age, :when, :country, :city, :contact_info, :comment, :kind)
  end
end
