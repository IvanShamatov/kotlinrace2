class ReportsController < ApplicationController

  # GET /reports
  # GET /reports.json
  def index
    @reports = Report.order(date: :desc).page params[:page]
  end

  # GET /reports/1
  # GET /reports/1.json
  def show
    @report = Report.find(params[:id])
  end

end
