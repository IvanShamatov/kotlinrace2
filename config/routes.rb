Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  resources :finishers, only: [:index]
  resources :reports, only: [:index, :show]
  get 'gallery' => "gallery#index"
  get 'register' => "register#new"
  post 'apply' => "register#create", as: :apply_form 
  match ":url" => 'pages#show', via: :get

  root "pages#home"
end
