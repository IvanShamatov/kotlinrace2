class CreateReports < ActiveRecord::Migration
  def change
    create_table :reports do |t|
      t.string :title
      t.date :date
      t.string :author
      t.text :body
      t.string :cover_image

      t.timestamps null: false
    end
  end
end
