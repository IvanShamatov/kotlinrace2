class CreateStarts < ActiveRecord::Migration
  def change
    create_table :starts do |t|
      t.date :date
      t.string :time
      t.boolean :wetsuit
      t.string :kind
      t.integer :finisher_id

      t.timestamps null: false
    end
    remove_column :finishers, :date
    remove_column :finishers, :time
    remove_column :finishers, :wetsuit
  end
end
