class RemoveReportIdFromStarts < ActiveRecord::Migration
  def change
    remove_column :starts, :report_id
  end
end
