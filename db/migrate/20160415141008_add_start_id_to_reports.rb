class AddStartIdToReports < ActiveRecord::Migration
  def change
    add_column :reports, :start_id, :integer
  end
end
