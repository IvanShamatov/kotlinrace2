class CreateFinishers < ActiveRecord::Migration
  def change
    create_table :finishers do |t|
      t.string :first_name
      t.string :last_name
      t.string :middle_name
      t.date :date
      t.string :time

      t.timestamps null: false
    end
  end
end
