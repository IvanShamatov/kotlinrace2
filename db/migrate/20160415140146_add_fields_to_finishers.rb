class AddFieldsToFinishers < ActiveRecord::Migration
  def change
    add_column :finishers, :photo, :string
    add_column :finishers, :about, :text
    add_column :finishers, :sport, :string
    add_column :starts, :whether, :text
    add_column :starts, :food, :text
    add_column :starts, :report_id, :integer
    add_column :images, :start_id, :integer
  end
end
