class CreateRequests < ActiveRecord::Migration
  def change
    create_table :requests do |t|
      t.string :name
      t.integer :age
      t.string :when
      t.string :country
      t.string :city
      t.string :contact_info
      t.text :comment
      t.string :kind

      t.timestamps null: false
    end
  end
end
