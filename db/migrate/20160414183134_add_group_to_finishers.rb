class AddGroupToFinishers < ActiveRecord::Migration
  def change
    add_column :finishers, :country, :string
    add_column :finishers, :city, :string
    add_column :finishers, :age, :integer
    add_column :finishers, :gender, :string
    add_column :finishers, :wetsuit, :boolean
  end
end
